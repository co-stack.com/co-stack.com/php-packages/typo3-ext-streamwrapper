<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper\Tests\Proxy;

use CoStack\Typo3ExtStreamwrapper\ExtStreamWrapper;

use function array_pop;
use function explode;
use function implode;
use function ltrim;

class ExtStreamWrapperProxy extends ExtStreamWrapper
{
    private array $paths = [
        'core' => __DIR__ . '/../../vendor/typo3/cms-core',
    ];

    public function getExtensionPath(string $extension, string $path): string
    {
        $absolutePath = $this->paths[$extension] . '/' . ltrim($path, '/');
        return $this->resolveBackPaths($absolutePath);
    }

    private function resolveBackPaths(string $absolutePath): string
    {
        $pathParts = explode('/', $absolutePath);
        $resolvedPathParts = [];
        foreach ($pathParts as $pathPart) {
            if ($pathPart === '..') {
                array_pop($resolvedPathParts);
                continue;
            }
            $resolvedPathParts[] = $pathPart;
        }
        return implode('/', $resolvedPathParts);
    }
}
