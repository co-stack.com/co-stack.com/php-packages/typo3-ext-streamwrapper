<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper\Tests\Unit;

use CoStack\Typo3ExtStreamwrapper\ExtStreamWrapper;
use PHPUnit\Framework\TestCase;

use function stream_get_wrappers;
use function stream_wrapper_unregister;

/**
 * @coversNothing
 */
class ExtStreamWrapperRegistrationTest extends TestCase
{
    public function testThatTheStreamWrapperIsNotRegistered(): void
    {
        $wrappers = stream_get_wrappers();
        $this->assertNotContains('EXT', $wrappers);
    }

    public function testThatTheStreamWrapperCanBeRegistered(): void
    {
        stream_wrapper_register('EXT', ExtStreamWrapper::class);
        $wrappers = stream_get_wrappers();
        stream_wrapper_unregister('EXT');
        $this->assertContains('EXT', $wrappers);
    }
}
