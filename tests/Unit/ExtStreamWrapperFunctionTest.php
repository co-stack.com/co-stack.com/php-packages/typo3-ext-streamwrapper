<?php
/**
 * @noinspection UnknownInspectionInspection
 */

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper\Tests\Unit;

use CoStack\Typo3ExtStreamwrapper\Tests\Proxy\ExtStreamWrapperProxy;
use PHPUnit\Framework\TestCase;

use function chgrp;
use function chmod;
use function chown;
use function closedir;
use function fclose;
use function feof;
use function file_exists;
use function file_get_contents;
use function flock;
use function fopen;
use function fread;
use function fstat;
use function ftell;
use function ftruncate;
use function fwrite;
use function opendir;
use function readdir;
use function rename;
use function rewinddir;
use function rmdir;
use function sort;
use function stream_get_meta_data;
use function stream_set_blocking;
use function stream_set_read_buffer;
use function stream_set_timeout;
use function stream_set_write_buffer;
use function stream_wrapper_register;
use function stream_wrapper_unregister;
use function touch;
use function unlink;

use const LOCK_SH;

/**
 * @coversDefaultClass \CoStack\Typo3ExtStreamwrapper\ExtStreamWrapper
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ExtStreamWrapperFunctionTest extends TestCase
{
    public function setUp(): void
    {
        stream_wrapper_register('EXT', ExtStreamWrapperProxy::class);
    }

    public function tearDown(): void
    {
        stream_wrapper_unregister('EXT');
        if (file_exists(__DIR__ . '/../../vendor/typo3/cms-core/_testing.txt')) {
            unlink(__DIR__ . '/../../vendor/typo3/cms-core/_testing.txt');
        }
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_stat
     * @covers ::stream_read
     * @covers ::stream_eof
     */
    public function testFileGetContent(): void
    {
        $contents = file_get_contents('EXT://core/LICENSE.txt');
        $this->assertStringEqualsFile(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt', $contents);
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_stat
     * @covers ::stream_eof
     * @covers ::stream_read
     */
    public function testReadingHandle(): void
    {
        $handle = fopen('EXT://core/LICENSE.txt', 'rb');
        $this->assertIsResource($handle);

        $position = ftell($handle);
        $this->assertSame(0, $position);

        $isEof = feof($handle);
        $this->assertFalse($isEof);

        $content = fread($handle, 131);
        $this->assertSame(
            '                    GNU GENERAL PUBLIC LICENSE
                       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software',
            $content,
        );

        $position = ftell($handle);
        $this->assertSame(131, $position);

        fclose($handle);
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_seek
     * @covers ::stream_write
     * @covers ::stream_flush
     * @covers ::unlink
     * @covers ::stream_truncate
     */
    public function testWritingNewHandle(): void
    {
        $handle = fopen('EXT://core/test.txt', 'ab');

        $textString = "Welcome to the internet\nThere is no place like 127.0.0.1";
        fwrite($handle, $textString);

        $this->assertStringEqualsFile(__DIR__ . '/../../vendor/typo3/cms-core/test.txt', $textString);

        ftruncate($handle, 7);

        $this->assertStringEqualsFile(__DIR__ . '/../../vendor/typo3/cms-core/test.txt', 'Welcome');

        fclose($handle);

        unlink('EXT://core/test.txt');
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_stat
     * @covers ::stream_eof
     * @covers ::stream_read
     */
    public function testReadingFilesSimultaneously(): void
    {
        $licenseHandle = fopen('EXT://core/LICENSE.txt', 'rb');
        $readmeHandle = fopen('EXT://core/README.rst', 'rb');

        $firstLicenseBytes = fread($licenseHandle, 20);
        $firstReadmeBytes = fread($readmeHandle, 20);

        fclose($licenseHandle);
        fclose($readmeHandle);

        $this->assertSame('                    ', $firstLicenseBytes);
        $this->assertSame('====================', $firstReadmeBytes);
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_eof
     * @covers ::stream_stat
     */
    public function testFstat(): void
    {
        $licenseHandle = fopen('EXT://core/LICENSE.txt', 'rb');
        $stat = fstat($licenseHandle);

        $this->assertSame(18092, $stat['size']);
    }

    /**
     * @covers ::mkdir
     * @covers ::rmdir
     * @covers ::expandPath
     */
    public function testMkdirAndRmdir(): void
    {
        $this->assertDirectoryDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/_test_folder');
        mkdir('EXT://core/_test_folder');
        $this->assertDirectoryExists(__DIR__ . '/../../vendor/typo3/cms-core/_test_folder');
        rmdir('EXT://core/_test_folder');
        $this->assertDirectoryDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/_test_folder');
    }

    /**
     * @covers ::dir_opendir
     * @covers ::expandPath
     * @covers ::dir_readdir
     * @covers ::dir_closedir
     */
    public function testReaddir(): void
    {
        $handle = opendir('EXT://core/');
        $dirContents = [];
        while ($dirContent = readdir($handle)) {
            $dirContents[] = $dirContent;
        }
        $actualContents = [
            '.',
            '..',
            'Classes',
            'Configuration',
            'Documentation',
            'Resources',
            'composer.json',
            'ext_emconf.php',
            'ext_localconf.php',
            'ext_tables.sql',
            'LICENSE.txt',
            'README.rst',
        ];
        sort($dirContents);
        sort($actualContents);
        $this->assertSame($actualContents, $dirContents);

        $this->assertFalse(readdir($handle));

        closedir($handle);
    }

    /**
     * @covers ::dir_opendir
     * @covers ::expandPath
     * @covers ::dir_readdir
     * @covers ::dir_rewinddir
     * @covers ::dir_closedir
     */
    public function testRewinddir(): void
    {
        $handle = opendir('EXT://core/Classes/Adapter');
        /** @noinspection LoopWhichDoesNotLoopInspection */
        /** @noinspection MissingOrEmptyGroupStatementInspection */
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        while (readdir($handle)) {
            // loop to the end
        }

        $this->assertFalse(readdir($handle));

        rewinddir($handle);

        // Actual value depends on the file creation time
        $this->assertContains(readdir($handle), ['.', '..', 'EventDispatcherAdapter.php']);

        closedir($handle);
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_eof
     * @covers ::stream_set_option
     */
    public function testStreamMetaData(): void
    {
        $handle = fopen('EXT://core/LICENSE.txt', 'rb');

        stream_set_blocking($handle, false);
        stream_set_write_buffer($handle, 256);
        stream_set_read_buffer($handle, 128);
        stream_set_timeout($handle, 1, 100);

        $meta = stream_get_meta_data($handle);

        $this->assertFalse($meta['timed_out']);
        $this->assertTrue($meta['blocked']);
        $this->assertFalse($meta['eof']);
        $this->assertSame('user-space', $meta['wrapper_type']);
        $this->assertSame('user-space', $meta['stream_type']);
        $this->assertSame('rb', $meta['mode']);
        $this->assertTrue($meta['seekable']);
        $this->assertSame('EXT://core/LICENSE.txt', $meta['uri']);

        fclose($handle);
    }

    /**
     * @covers ::stream_metadata
     * @covers ::expandPath
     * @covers ::unlink
     */
    public function testFileMetaFunctions(): void
    {
        $this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/_testing.txt');
        touch('EXT://core/_testing.txt');
        touch('EXT://core/_testing.txt', 1697812156);
        /** @noinspection PotentialMalwareInspection */
        touch('EXT://core/_testing.txt', 1697812156, 1697812160);
        $this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/_testing.txt');

        chmod('EXT://core/_testing.txt', 0664);
        chown('EXT://core/_testing.txt', 1000);
        chgrp('EXT://core/_testing.txt', 1000);

        unlink('EXT://core/_testing.txt');
    }

    /**
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_lock
     */
    public function testLocking(): void
    {
        $handle = fopen('EXT://core/LICENSE.txt', 'rb');
        $wouldBlock = null;
        $locked = flock($handle, LOCK_SH, $wouldBlock);
        $this->assertTrue($locked);
        $this->assertSame(0, $wouldBlock);
        fclose($handle);
    }

    /**
     * @covers ::rename
     * @covers ::expandPath
     */
    public function testRename(): void
    {
        $this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');
        $this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');

        rename('EXT://core/LICENSE.txt', 'EXT://core/LICENSE-testing.txt');

        $this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');
        $this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');

        rename('EXT://core/LICENSE-testing.txt', 'EXT://core/LICENSE.txt');

        $this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');
        $this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');

        // Following tests are not supported by PHP, but they would be by the stream wrapper
        //rename(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt', 'EXT://core/LICENSE-testing.txt');

        //$this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');
        //$this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');

        //rename('EXT://core/LICENSE-testing.txt', __DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');

        //$this->assertFileDoesNotExist(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');
        //$this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE.txt');
    }

    /**
     * @covers ::url_stat
     * @covers ::stream_open
     * @covers ::expandPath
     * @covers ::stream_eof
     * @covers ::stream_read
     * @covers ::stream_write
     * @covers ::stream_flush
     * @covers ::unlink
     */
    public function testStatFunctions(): void
    {
        copy('EXT://core/LICENSE.txt', 'EXT://core/LICENSE-testing.txt');
        $this->assertFileExists(__DIR__ . '/../../vendor/typo3/cms-core/LICENSE-testing.txt');

        unlink('EXT://core/LICENSE-testing.txt');
    }
}
