<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper\Tests\Unit\Exception;

use CoStack\Typo3ExtStreamwrapper\Exception\StreamWrapperAlreadyRegisteredException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\Typo3ExtStreamwrapper\Exception\StreamWrapperAlreadyRegisteredException
 */
class StreamWrapperAlreadyRegisteredExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testExceptionMessageAndCode(): void
    {
        $exception = new StreamWrapperAlreadyRegisteredException('FOO');
        $this->assertSame(1697806635, $exception->getCode());
        $this->assertSame('A stream wrapper for protocol "FOO" has already been registered', $exception->getMessage());
    }
}
