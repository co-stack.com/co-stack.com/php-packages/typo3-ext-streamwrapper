<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper\Exception;

use CoStack\Typo3ExtStreamwrapper\Typo3ExtStreamWrapperException;
use JetBrains\PhpStorm\Pure;
use Throwable;

use function sprintf;

class StreamWrapperAlreadyRegisteredException extends Typo3ExtStreamWrapperException
{
    private const MESSAGE = 'A stream wrapper for protocol "%s" has already been registered';
    final public const CODE = 1697806635;

    #[Pure]
    public function __construct(string $protocol, ?Throwable $previous = null)
    {
        parent::__construct(sprintf(self::MESSAGE, $protocol), self::CODE, $previous);
    }

}
