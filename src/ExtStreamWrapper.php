<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

use function chgrp;
use function chmod;
use function chown;
use function count;
use function str_starts_with;
use function stream_set_blocking;
use function stream_set_read_buffer;
use function stream_set_timeout;
use function stream_set_write_buffer;
use function touch;

use const STREAM_META_ACCESS;
use const STREAM_META_GROUP;
use const STREAM_META_GROUP_NAME;
use const STREAM_META_OWNER;
use const STREAM_META_OWNER_NAME;
use const STREAM_META_TOUCH;
use const STREAM_OPTION_READ_BUFFER;
use const STREAM_OPTION_READ_TIMEOUT;
use const STREAM_OPTION_WRITE_BUFFER;
use const STREAM_URL_STAT_LINK;
use const STREAM_URL_STAT_QUIET;

/**
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ExtStreamWrapper
{
    /** @var null|resource */
    public $context;
    /** @var null|resource */
    public $handle;

    /**
     * @see http://php.net/manual/de/streamwrapper.dir-opendir.php
     * Open directory handle
     *
     * @param string $path
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     * @codingStandardsIgnoreStart
     */
    public function dir_opendir(string $path): bool
    {
        // @codingStandardsIgnoreEnd
        $this->handle = opendir($this->expandPath($path), $this->context);
        return is_resource($this->context);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.dir-readdir.php
     * Read entry from directory handle
     *
     * @return string|false
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function dir_readdir(): string|false
    {
        // @codingStandardsIgnoreEnd
        return readdir($this->handle);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.dir-closedir.php
     * Close directory handle
     *
     * The documentation says this method should return a bool but there is nothing to return!
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function dir_closedir(): void
    {
        // @codingStandardsIgnoreEnd
        closedir($this->handle);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.dir-rewinddir.php
     * Rewind directory handle
     *
     * @return void
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function dir_rewinddir(): void
    {
        // @codingStandardsIgnoreEnd
        rewinddir($this->handle);
    }

    /**
     * The fourth bit in options (int 8) is always set, but not documented. So it's ignored!
     *
     * @see http://php.net/manual/de/streamwrapper.mkdir.php
     * Create a directory
     *
     * @param string $path
     * @param int $mode
     * @param int $options
     * @return bool
     */
    public function mkdir(string $path, int $mode, int $options): bool
    {
        /*
         * GeneralUtility::mkdir is not used, because $mode would be overwritten by it.
         * A Stream specific option to use GU instead will most likely come ;)
         */
        return mkdir($this->expandPath($path), $mode, (bool)($options & STREAM_MKDIR_RECURSIVE), $this->context);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.rename.php
     * Renames a file or directory
     *
     * @param string $oldPath
     * @param string $newPath
     * @return bool
     */
    public function rename(string $oldPath, string $newPath): bool
    {
        if (str_starts_with($oldPath, 'EXT://')) {
            $oldPath = $this->expandPath($oldPath);
        }
        if (str_starts_with($newPath, 'EXT://')) {
            $newPath = $this->expandPath($newPath);
        }
        return rename($oldPath, $newPath, $this->context);
    }

    /**
     * Yeah, $options could contain STREAM_MKDIR_RECURSIVE... What the actual f***?
     *
     * @see http://php.net/manual/de/streamwrapper.rmdir.php
     * Removes a directory
     *
     * @param string $path
     * @return bool
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     */
    public function rmdir(string $path): bool
    {
        return rmdir($this->expandPath($path), $this->context);
    }

    /**
     * I know about STREAM_REPORT_ERRORS, but I still do not feel
     * responsible for suppressing the default error just to
     * check for it and "rethrow" the error afterward.
     *
     * @see http://php.net/manual/de/streamwrapper.stream-open.php
     * Opens file or URL
     *
     * @param string $path
     * @param string $mode
     * @param int $options
     * @param string|null $openedPath
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_open(string $path, string $mode, int $options, ?string &$openedPath): bool
    {
        // @codingStandardsIgnoreEnd
        $absolutePath = $this->expandPath($path);
        $this->handle = fopen($absolutePath, $mode);

        $success = is_resource($this->handle);

        if (STREAM_USE_PATH === ($options & STREAM_USE_PATH) && $success) {
            $openedPath = $absolutePath;
        }

        return $success;
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-eof.php
     * Tests for end-of-file on a file pointer
     *
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_eof(): bool
    {
        // @codingStandardsIgnoreEnd
        return feof($this->handle);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-flush.php
     * Flushes the output
     *
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_flush(): bool
    {
        // @codingStandardsIgnoreEnd
        return fflush($this->handle);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-metadata.php
     * Change stream options
     *
     * @param string $path
     * @param int $option
     * @param mixed $value
     * @return bool
     *
     * @noinspection PotentialMalwareInspection
     * @noinspection UnknownInspectionInspection
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_metadata(string $path, int $option, mixed $value): bool
    {
        // @codingStandardsIgnoreEnd
        $path = $this->expandPath($path);
        return match ($option) {
            STREAM_META_TOUCH => count($value) > 0 ? touch($path, $value[0], $value[1]) : touch($path),
            STREAM_META_OWNER, STREAM_META_OWNER_NAME => chown($path, $value),
            STREAM_META_GROUP, STREAM_META_GROUP_NAME => chgrp($path, $value),
            STREAM_META_ACCESS => chmod($path, $value),
            default => false,
        };
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-lock.php
     * Advisory file locking
     *
     * @param int $operation
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_lock(int $operation): bool
    {
        // @codingStandardsIgnoreEnd
        return flock($this->handle, $operation);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-read.php
     * Read from stream
     *
     * @param int $length
     * @return string
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_read(int $length): string
    {
        // @codingStandardsIgnoreEnd
        return fread($this->handle, $length);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-seek.php
     * Seeks to specific location in a stream
     *
     * @param int $offset
     * @param int $whence
     * @return int
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_seek(int $offset, int $whence = SEEK_SET): int
    {
        // @codingStandardsIgnoreEnd
        return fseek($this->handle, $offset, $whence);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-set-option.php
     * Change stream options
     *
     * @param int $option
     * @param int $arg1
     * @param int|null $arg2
     * @return int|bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_set_option(int $option, int $arg1, ?int $arg2): int|bool
    {
        return match ($option) {
            STREAM_OPTION_BLOCKING => stream_set_blocking($this->handle, (bool)$arg1),
            STREAM_OPTION_READ_TIMEOUT => stream_set_timeout($this->handle, $arg1, $arg2),
            STREAM_OPTION_READ_BUFFER => stream_set_read_buffer($this->handle, $arg2),
            STREAM_OPTION_WRITE_BUFFER => stream_set_write_buffer($this->handle, $arg2),
            default => false,
        };
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-stat.php
     * Retrieve information about a file resource
     *
     * @return array
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_stat(): array
    {
        // @codingStandardsIgnoreEnd
        return fstat($this->handle);
    }

    /**
     * The docs are a lie! This method is NOT called in response
     * to fseek(), instead it is only triggered by ftell()!
     * BTW: ftell() works without this implementation.
     * WTF! Implemented anyway.
     * FFS.
     *
     * @see http://php.net/manual/de/streamwrapper.stream-tell.php
     * Retrieve the current position of a stream
     *
     * @return int
     *
     * @codeCoverageIgnore
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_tell(): int
    {
        // @codingStandardsIgnoreEnd
        return ftell($this->handle);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-truncate.php
     * Truncate stream
     *
     * @param int $limit
     * @return bool
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_truncate(int $limit): bool
    {
        // @codingStandardsIgnoreEnd
        return ftruncate($this->handle, $limit);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.stream-write.php
     * Write to stream
     *
     * @param string $data
     * @return int
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function stream_write(string $data): int
    {
        // @codingStandardsIgnoreEnd
        return fwrite($this->handle, $data);
    }

    /**
     * @http://php.net/manual/de/streamwrapper.unlink.php
     * Delete a file
     *
     * @param string $path
     * @return bool
     */
    public function unlink(string $path): bool
    {
        return unlink($this->expandPath($path), $this->context);
    }

    /**
     * @see http://php.net/manual/de/streamwrapper.url-stat.php
     * Retrieve information about a file
     *
     * @param string $path
     * @param int $flags
     * @return array|false
     *
     * @SuppressWarnings("PHPMD.CamelCaseMethodName")
     * @codingStandardsIgnoreStart
     */
    public function url_stat(string $path, int $flags): array|false
    {
        // @codingStandardsIgnoreEnd
        $path = $this->expandPath($path);

        $quiet = $flags & STREAM_URL_STAT_QUIET;

        if ($flags & STREAM_URL_STAT_LINK) {
            if ($quiet) {
                return @lstat($path);
            }

            return lstat($path);
        }

        if ($quiet) {
            return @stat($path);
        }

        return stat($path);
    }

    /**
     * Always aware of multibyte strings!
     *
     * Replaces the "EXT://EXTKEY" part with the real path to the extension folder
     *
     * @param string $path
     * @return string
     */
    public function expandPath(string $path): string
    {
        // cut away "EXT://"
        $rawPath = mb_substr($path, 6);

        // split key and trailing path at first slash
        $pos = mb_strpos($rawPath, '/') ?: null;
        if (null === $pos) {
            $key = mb_substr($rawPath, 0);
            $rest = '';
        } else {
            $key = mb_substr($rawPath, 0, $pos);
            $rest = mb_substr($rawPath, $pos + 1);
        }

        return $this->getExtensionPath($key, $rest);
    }

    /**
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getExtensionPath(string $extension, string $path): string
    {
        return ExtensionManagementUtility::extPath($extension, $path);
    }
}
