<?php

declare(strict_types=1);

namespace CoStack\Typo3ExtStreamwrapper;

use Exception;

abstract class Typo3ExtStreamWrapperException extends Exception
{

}
